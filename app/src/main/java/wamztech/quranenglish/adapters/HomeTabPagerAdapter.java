package wamztech.quranenglish.adapters;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import wamztech.quranenglish.fragments.SearchFragment;
import wamztech.quranenglish.fragments.SiparayFragment;
import wamztech.quranenglish.fragments.SuratFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class HomeTabPagerAdapter extends FragmentStatePagerAdapter {



    public HomeTabPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {


            case 1:
                return SiparayFragment.newInstance();
            case 2:
                return SearchFragment.newInstance();
            default:
                return SuratFragment.newInstance();
        }

    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        if (position == 0) {

            return "SURAH";
        } else if (position == 1) {
            return "PARA";
        } else return "SEARCH";


    }
}