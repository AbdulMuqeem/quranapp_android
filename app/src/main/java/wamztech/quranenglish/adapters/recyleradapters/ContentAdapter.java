package wamztech.quranenglish.adapters.recyleradapters;

import android.app.Activity;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ViewHolder> {

    public boolean isClickable = true;
    private final OnItemAdapterClickListener onItemClick;
    private Activity activity;
    private ArrayList<ContentModel> arrayList;

    public ContentAdapter(Activity activity, ArrayList arrayList, OnItemAdapterClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ContentModel model = arrayList.get(holder.getAdapterPosition());
        if (position == 0) {
            holder.imgShare.setVisibility(View.GONE);
            holder.imgBookMark.setVisibility(View.GONE);
        }
        holder.txtContent.setText(model.getArabic());
        holder.txtAyatCount.setText(model.getAyatNo() + "");
        holder.txtSipareEn.setText(model.getEng());
        if (model.isSelected()) {
            holder.contContent.setBackground(activity.getResources().getDrawable(R.drawable.strokesquare));
        } else {
            holder.contContent.setBackground(activity.getResources().getDrawable(R.drawable.square));
        }


        setListener(holder, model);
    }

    private void setListener(ViewHolder holder, ContentModel model) {


        holder.imgBookMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(null, v, holder.getAdapterPosition(), model);
            }
        });

        holder.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(null, v, holder.getAdapterPosition(), model);
            }
        });
        holder.contContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!isClickable) return;

                holder.contContent.setBackground(activity.getResources().getDrawable(R.drawable.strokesquare));
                onItemClick.onItemClick(null, v, holder.getAdapterPosition(), model);
//                isClickable = false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgBookMark)
        ImageView imgBookMark;
        @BindView(R.id.imgShare)
        ImageView imgShare;
        @BindView(R.id.txtContent)
        AnyTextView txtContent;
        @BindView(R.id.txtSipareEn)
        AnyTextView txtSipareEn;
        @BindView(R.id.txtAyatCount)
        AnyTextView txtAyatCount;
        @BindView(R.id.contContent)
        LinearLayout contContent;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
