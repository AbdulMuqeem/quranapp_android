package wamztech.quranenglish.adapters.recyleradapters;

import android.app.Activity;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jcminarro.roundkornerlayout.RoundKornerFrameLayout;

import wamztech.quranenglish.R;
import wamztech.quranenglish.callbacks.OnItemClickListener;
import wamztech.quranenglish.models.QuranChapterandVerses;
import wamztech.quranenglish.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class SiparayNameAdapter extends RecyclerView.Adapter<SiparayNameAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;


    private Activity activity;
    private ArrayList<QuranChapterandVerses> arrayList;

    public SiparayNameAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_siparay_name, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final QuranChapterandVerses model = arrayList.get(holder.getAdapterPosition());
        holder.txtSipareAr.setText(model.getChapterNameAr());
        holder.txtIndex.setText(model.getChapterNumber());
        holder.txtSipareEn.setText(model.getChapterNameEng());
        setListener(holder, model);
    }

    private void setListener(ViewHolder holder, QuranChapterandVerses model) {
        holder.contLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), model);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtSipareAr)
        AnyTextView txtSipareAr;
        @BindView(R.id.txtIndex)
        AnyTextView txtIndex;
        @BindView(R.id.txtSipareEn)
        AnyTextView txtSipareEn;
        @BindView(R.id.contChildLL)
        LinearLayout contChildLL;
        @BindView(R.id.contLL)
        LinearLayout contLL;
        @BindView(R.id.contRL)
        RoundKornerFrameLayout contRL;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
