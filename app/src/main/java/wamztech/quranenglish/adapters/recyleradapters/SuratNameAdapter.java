package wamztech.quranenglish.adapters.recyleradapters;

import android.annotation.SuppressLint;
import android.app.Activity;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.callbacks.OnItemClickListener;
import wamztech.quranenglish.models.QuranChapterandVerses;
import wamztech.quranenglish.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class SuratNameAdapter extends RecyclerView.Adapter<SuratNameAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;



    private Activity activity;
    private ArrayList<QuranChapterandVerses> arrayList;

    public SuratNameAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_surat, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final QuranChapterandVerses model = arrayList.get(holder.getAdapterPosition());
        holder.txtTitleAr.setText(model.getDetail());
        holder.txtTitleEn.setText(model.getChapterNameEng());
        holder.txtCount.setText(position + 1 + ".");
        holder.txtDetail.setText(model.getVerseTotalCount() + " - " + model.getOrigin());
        setListener(holder, model);
    }

    private void setListener(ViewHolder holder, QuranChapterandVerses model) {
        holder.contChildLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), model);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtCount)
        AnyTextView txtCount;
        @BindView(R.id.txtTitleEn)
        AnyTextView txtTitleEn;
        @BindView(R.id.txtDetail)
        AnyTextView txtDetail;
        @BindView(R.id.txtTitleAr)
        AnyTextView txtTitleAr;
        @BindView(R.id.contChildLL)
        LinearLayout contChildLL;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
