package wamztech.quranenglish.adapters.recyleradapters;

import android.app.Activity;
import android.graphics.Color;


import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import wamztech.quranenglish.R;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {


    private final OnItemAdapterClickListener onItemClick;

    private ArrayList<ContentModel> arrayList;
    private Activity activity;
    private ArrayList<ContentModel> filteredData = new ArrayList<>();
    private Filter mFilter = new ItemFilter();
    private String searchString = "";

    public SearchAdapter(Activity activity, ArrayList arrayList, OnItemAdapterClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.filteredData = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_bookmarked, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ContentModel model = filteredData.get(holder.getAdapterPosition());
        holder.btnCross.setVisibility(View.GONE);
        holder.txtContent.setText(model.getArabic());
        holder.txtSipareEn.setText(model.getEng());
        holder.txtBookedmarked.setVisibility(View.GONE);
        setListener(holder, model);

        searchTextColor(holder,model);
    }

    private void searchTextColor(ViewHolder holder, ContentModel model) {
        // Find charText in wp
        String country = model.getEng();
        if (country.contains(searchString)) {
            Log.e("test", country + " contains: " + searchString);
            int startPos = country.indexOf(searchString);
            int endPos = startPos + searchString.length();

            Spannable spanText = Spannable.Factory.getInstance().newSpannable(holder.txtSipareEn.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
            spanText.setSpan(new ForegroundColorSpan(Color.YELLOW), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.txtSipareEn.setText(spanText, TextView.BufferType.SPANNABLE);}

    }

    private void setListener(ViewHolder holder, ContentModel model) {


        holder.btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(null, v, holder.getAdapterPosition(), model);
            }
        });

    }


    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnCross)
        ImageView btnCross;
        @BindView(R.id.txtContent)
        AnyTextView txtContent;
        @BindView(R.id.contContent)
        LinearLayout contContent;
        @BindView(R.id.txtSipareEn)
        AnyTextView txtSipareEn;      @BindView(R.id.txtBookedmarked)
        AnyTextView txtBookedmarked;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }



    public Filter getFilter() {

        return mFilter;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<ContentModel> list = arrayList;

            int count = list.size();

            final ArrayList<ContentModel> filterData = new ArrayList<ContentModel>();

            String filterableString1;
            String filterableString2;

            for (int i = 0; i < count; i++) {
                filterableString1 = list.get(i).getArabic();
       searchString =         filterableString2 = list.get(i).getEng();
                if (filterableString1.toLowerCase().contains(filterString)
                        || filterableString2.toLowerCase().contains(filterString)
                ) {
                    filterData.add(list.get(i));
                }
            }

            results.values = filterData;
            results.count = filterData.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<ContentModel>) results.values;
            notifyDataSetChanged();
        }

    }


    public int getCount() {
        if (filteredData == null) {
            return 0;
        }
        return filteredData.size();
    }

    public ContentModel getItem(int position) {
        return filteredData.get(position);
    }

}
