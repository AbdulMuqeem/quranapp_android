package wamztech.quranenglish.adapters.recyleradapters;

import android.app.Activity;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class BookMarkAdapter extends RecyclerView.Adapter<BookMarkAdapter.ViewHolder> {


    private final OnItemAdapterClickListener onItemClick;

    private ArrayList<ContentModel> arrayList;
    private Activity activity;

    public BookMarkAdapter(Activity activity, ArrayList arrayList, OnItemAdapterClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_bookmarked, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ContentModel model = arrayList.get(holder.getAdapterPosition());
        holder.txtContent.setText(model.getArabic());
        holder.txtSipareEn.setText(model.getEng());
        holder.txtBookedmarked.setText(model.getTextValue());
        setListener(holder, model);
    }

    private void setListener(ViewHolder holder, ContentModel model) {


        holder.btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(null, v, holder.getAdapterPosition(), model);
            }
        });
        holder.contContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(null, v, holder.getAdapterPosition(), model);
            }
        });

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnCross)
        ImageView btnCross;
        @BindView(R.id.txtContent)
        AnyTextView txtContent;
        @BindView(R.id.contContent)
        LinearLayout contContent;
        @BindView(R.id.txtSipareEn)
        AnyTextView txtSipareEn;
        @BindView(R.id.txtBookedmarked)
        AnyTextView txtBookedmarked;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
