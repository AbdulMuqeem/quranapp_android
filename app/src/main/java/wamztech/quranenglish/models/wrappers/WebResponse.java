package wamztech.quranenglish.models.wrappers;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khanhamza on 09-Mar-17.
 */

public class WebResponse<T> {

    @SerializedName("message")
    public String message;

    @SerializedName("response")
    public int responseCode;

//    @SerializedName("ResponseType")
//    public String responseType;

    @SerializedName("data")
    public T result;

    public boolean isSuccess() {
        return responseCode == 200;
    }


    public boolean noRecordFound() {
        return responseCode == 204;
    }
}
