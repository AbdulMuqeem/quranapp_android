package wamztech.quranenglish.models;

public class ContentModel {
    private int versesEndsTo;
    private int versesStartFrom;
    private int ayatNo;
    private String ChapterNumber;
    private int suratNo;
    //    private String content;
    private String arabic, eng;
    private String Name,textValue;
    private int isFromSipara;
    boolean isSelected, isArabic = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ContentModel(/*String textValue, */String chapterNumber, int suratNo, int ayatNo, String arabic, String eng) {
        this.ayatNo = ayatNo;
        ChapterNumber = chapterNumber;
//        textValue = textValue;
        this.suratNo = suratNo;
        this.arabic = arabic;
        this.eng = eng;
    }

    public ContentModel(String textValue,String chapterNumber, int suratNo, int ayatNo, String arabic, String eng, String name, int versesStartFrom, int versesEndsTo) {
        this.ayatNo = ayatNo;
        this.ChapterNumber = chapterNumber;
        this.textValue = textValue;
        this.suratNo = suratNo;
        this.arabic = arabic;
        this.eng = eng;
        this.Name = name;
        this.versesEndsTo = versesEndsTo;
        this.versesStartFrom = versesStartFrom;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setFromSipara(int fromSipara) {
        isFromSipara = fromSipara;
    }

    public int getAyatNo() {
        return ayatNo;
    }

    public void setAyatNo(int ayatNo) {
        this.ayatNo = ayatNo;
    }

    public String getChapterNumber() {
        return ChapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        ChapterNumber = chapterNumber;
    }

    public int getSuratNo() {
        return suratNo;
    }

    public void setSuratNo(int suratNo) {
        this.suratNo = suratNo;
    }

//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getEng() {
        return eng;
    }

    public void setEng(String eng) {
        this.eng = eng;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public boolean isArabic() {
        return isArabic;
    }

    public void setArabic(boolean arabic) {
        isArabic = arabic;
    }

    public int getVersesEndsTo() {
        return versesEndsTo;
    }

    public void setVersesEndsTo(int versesEndsTo) {
        this.versesEndsTo = versesEndsTo;
    }

    public int getVersesStartFrom() {
        return versesStartFrom;
    }

    public void setVersesStartFrom(int versesStartFrom) {
        this.versesStartFrom = versesStartFrom;
    }
}
