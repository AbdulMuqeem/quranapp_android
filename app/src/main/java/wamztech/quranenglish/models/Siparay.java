package wamztech.quranenglish.models;

/**
 * Created by aqsa.sarwar on 7/5/2019.
 */

public class Siparay {

    private String SiparaEn;
    private String SiparaAr;
    private int start, end, siparaNo;

    public Siparay(String siparaEn, String siparaAr) {
        SiparaEn = siparaEn;
        SiparaAr = siparaAr;
    }

    public Siparay(String siparaEn, String siparaAr, int start, int end, int siparaNo) {
        SiparaEn = siparaEn;
        SiparaAr = siparaAr;
        this.start = start;
        this.end = end;
        this.siparaNo = siparaNo;
    }

    public int getSiparaNo() {
        return siparaNo;
    }

    public void setSiparaNo(int siparaNo) {
        this.siparaNo = siparaNo;
    }

    public String getSiparaEn() {
        return SiparaEn;
    }

    public void setSiparaEn(String siparaEn) {
        SiparaEn = siparaEn;
    }

    public String getSiparaAr() {
        return SiparaAr;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end ;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public void setSiparaAr(String siparaAr) {
        SiparaAr = siparaAr;
    }
}
