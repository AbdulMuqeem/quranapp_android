package wamztech.quranenglish.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import wamztech.quranenglish.managers.retrofit.GsonFactory;

import java.io.Serializable;
import java.util.ArrayList;

public class SpinnerList implements Serializable
{

@SerializedName("arabic")
@Expose
private ArrayList<SpinnerDetail> arabic = null;
@SerializedName("english")
@Expose
private ArrayList<SpinnerDetail> english = null;
@SerializedName("urdu")
@Expose
private ArrayList<SpinnerDetail> urdu = null;
private final static long serialVersionUID = -1321178785743410046L;

public ArrayList<SpinnerDetail> getArabic() {
return arabic;
}

public void setArabic(ArrayList<SpinnerDetail> arabic) {
this.arabic = arabic;
}

public ArrayList<SpinnerDetail> getEnglish() {
return english;
}

public void setEnglish(ArrayList<SpinnerDetail> english) {
this.english = english;
}

public ArrayList<SpinnerDetail> getUrdu() {
return urdu;
}

public void setUrdu(ArrayList<SpinnerDetail> urdu) {
this.urdu = urdu;
}
    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}