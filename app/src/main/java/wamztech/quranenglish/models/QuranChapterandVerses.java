package wamztech.quranenglish.models;

public class QuranChapterandVerses {
    private String ChapterNameEng;
    private String ChapterNameAr;
    private String ChapterNumber;
    private String verseTotalCount;
    private int versesStartFrom;
    private int versesEndsTo;
    private String Origin;
    private String Detail;

    public QuranChapterandVerses(String chapterNumber,int versesStartFrom, int versesEndsTo,String chapterNameEng, String chapterNameAr) {
        ChapterNameEng = chapterNameEng;
        ChapterNameAr = chapterNameAr;
        ChapterNumber = chapterNumber;
        this.versesStartFrom = versesStartFrom;
        this.versesEndsTo = versesEndsTo;
    }

    public QuranChapterandVerses(String chapterNumber,int versesStartFrom, int versesEndsTo, String chapterNameEng, String verseTotalCount, String origin, String detail) {
        ChapterNameEng = chapterNameEng;
        ChapterNumber = chapterNumber;
        this.verseTotalCount = verseTotalCount;
        this.versesStartFrom = versesStartFrom;
        this.versesEndsTo = versesEndsTo;
        Origin = origin;
        Detail = detail;
    }

    public String getChapterNameEng() {
        return ChapterNameEng;
    }

    public void setChapterNameEng(String chapterNameEng) {
        ChapterNameEng = chapterNameEng;
    }

    public String getChapterNameAr() {
        return ChapterNameAr;
    }

    public void setChapterNameAr(String chapterNameAr) {
        ChapterNameAr = chapterNameAr;
    }

    public String getChapterNumber() {
        return ChapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        ChapterNumber = chapterNumber;
    }

    public String getVerseTotalCount() {
        return verseTotalCount;
    }

    public void setVerseTotalCount(String verseTotalCount) {
        this.verseTotalCount = verseTotalCount;
    }

    public int getVersesStartFrom() {
        return versesStartFrom;
    }

    public void setVersesStartFrom(int versesStartFrom) {
        this.versesStartFrom = versesStartFrom;
    }

    public int getVersesEndsTo() {
        return versesEndsTo;
    }

    public void setVersesEndsTo(int versesEndsTo) {
        this.versesEndsTo = versesEndsTo;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }
}
