package wamztech.quranenglish.models;

/**
 * Created by aqsa.sarwar on 7/5/2019.
 */

public class Surat {

    private  String chapter;
    private String Name;
    private String AyyatCount;
    private String Origin;
    private String Detail;
    private int start, end;
    public Surat(String name, String ayyatCount, String origin) {
        Name = name;
        AyyatCount = ayyatCount;
        Origin = origin;
    }
    public Surat(String name, String ayyatCount, String origin, String detail) {
        Name = name;
        AyyatCount = ayyatCount;
        Origin = origin;
        Detail = detail;
    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAyyatCount() {
        return AyyatCount;
    }

    public void setAyyatCount(String ayyatCount) {
        AyyatCount = ayyatCount;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public Surat(int start, int end, String name, String ayyatCount, String origin, String detail, String chapter) {
        Name = name;
        AyyatCount = ayyatCount;
        Origin = origin;
        Detail = detail;
        this.start = start;
        this.end = end;
        this.chapter = chapter;
    }
}
