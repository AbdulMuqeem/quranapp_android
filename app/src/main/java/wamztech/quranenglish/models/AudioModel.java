package wamztech.quranenglish.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import wamztech.quranenglish.managers.retrofit.GsonFactory;

/**
 * Created by aqsa.sarwar on 7/5/2019.
 */

public class AudioModel {

    @Expose
    @SerializedName("audio")
    private String audio;

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}
