package wamztech.quranenglish.callbacks;

public interface OnItemClickListener {
    void onItemClick(int position, Object object);
}
