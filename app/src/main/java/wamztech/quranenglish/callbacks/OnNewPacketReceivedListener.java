package wamztech.quranenglish.callbacks;

public interface OnNewPacketReceivedListener
{
    void onNewPacket(int event, Object data);
}
