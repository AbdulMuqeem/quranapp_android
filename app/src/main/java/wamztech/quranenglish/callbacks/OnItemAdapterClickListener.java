package wamztech.quranenglish.callbacks;

import android.view.View;

public interface OnItemAdapterClickListener {
    void onItemClick(String txtValue, View view, int position, Object object);
}
