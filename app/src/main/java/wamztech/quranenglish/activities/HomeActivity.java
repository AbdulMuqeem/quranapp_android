package wamztech.quranenglish.activities;

import android.os.Bundle;

import android.widget.RelativeLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.fragments.DashboardFragment;
import wamztech.quranenglish.helperclasses.ui.helper.UIHelper;
import wamztech.quranenglish.managers.SharedPreferenceManager;

import androidx.annotation.Nullable;


public class HomeActivity extends BaseActivity {


    RelativeLayout contParentActivityLayout;
    private SharedPreferenceManager sharedPreferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contParentActivityLayout = findViewById(R.id.contParentActivityLayout);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        initFragments(/*UserTypeEnum.fromCanonicalForm(userModel.getCssausermodel().getRole())*/);
    }

    public RelativeLayout getContParentActivityLayout() {
        return contParentActivityLayout;
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_home;
    }

    @Override
    protected int getTitlebarLayoutId() {
        return R.id.titlebar;
    }


    @Override
    protected int getDockableFragmentId() {
        return R.id.contMain;
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    // FIXME: 26/07/2018 Use Enum UserType instead of String
    private void initFragments() {
            addDockableFragment(DashboardFragment.newInstance(), false);

    }


    @Override
    public void onBackPressed() {
        if (isInSelectingState) {
            if (genericClickableInterface != null) {
                genericClickableInterface.click();
                isInSelectingState = false;
            } else {
                UIHelper.showToast(getBaseContext(), "No call back selected.");
            }
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            closeApp();
        }
    }




}