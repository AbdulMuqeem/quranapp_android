/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package wamztech.quranenglish.constatnts;

import android.content.Context;

import wamztech.quranenglish.models.QuranChapterandVerses;

import java.util.ArrayList;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public final class Constants {


    public static ArrayList<QuranChapterandVerses> arrSiparay(Context context) {
        ArrayList<QuranChapterandVerses> arrayList = new ArrayList<>();

        arrayList.add(new QuranChapterandVerses("1", 0, 296,"Alif Lam Meem", "الم"));          //1
        arrayList.add(new QuranChapterandVerses("2", 298, 518,"Sayaqool", "سَيَقُولُ"));             //2
        arrayList.add(new QuranChapterandVerses("3", 520, 770,"Tilkal Rusull", "تِلْكَ الرُّسُلُ"));    //3 295
        arrayList.add(new QuranChapterandVerses("4", 772, 1036,"Lan Tana Loo", "لَنْ تَنَالُوا"));     //4
        arrayList.add(new QuranChapterandVerses("5", 1038, 1284,"Wal Mohsanat", "وَالْمُحْصَنَاتُ"));     //5


        arrayList.add(new QuranChapterandVerses("6", 1286, 1508,"La Yuhibbullah", "لَا يُحِبُّ اللَّهُ")); //6
        arrayList.add(new QuranChapterandVerses("7", 1510, 1806,"Wa Iza Samiu", "وَإِذَا سَمِعُوا"));   //7
        arrayList.add(new QuranChapterandVerses("8", 1808, 2092,"Wa Lau Annana", "وَلَوْ أَنَّنَا"));    //8
        arrayList.add(new QuranChapterandVerses("9", 2094, 2412,"Qalal Malao", "قَالَ الْمَلَأُ"));      //9
        arrayList.add(new QuranChapterandVerses("10", 2414, 2668,"Wa A'lamu", "وَاعْلَمُوا"));         //10


        arrayList.add(new QuranChapterandVerses("11", 2670, 2972,"Yatazeroon", "يَعْتَذِرُون"));             //11
        arrayList.add(new QuranChapterandVerses("12", 2974, 3314,"Wa Mamin Da'abat", "وَمَا مِنْ دَابَّةٍ"));   //12
        arrayList.add(new QuranChapterandVerses("13", 3316, 3630,"Wa Ma Ubrioo", "وَمَا أُبَرِّئُ"));          //13
        arrayList.add(new QuranChapterandVerses( "14", 3632, 4068,"Rubama", "رُبَمَا"));                    //14
        arrayList.add(new QuranChapterandVerses( "15", 4088, 4358,"Subhanallazi", "سُبْحَانَ الَّذِي"));        //15


        arrayList.add(new QuranChapterandVerses( "16", 4360, 5002,"Qal Alam", "قَالَ أَلَمْ"));  //16
        arrayList.add(new QuranChapterandVerses( "17", 5004, 5386,"Aqtarabo", "اقْتَرَب"));  //17
        arrayList.add(new QuranChapterandVerses( "18", 5388, 5794,"Qadd Aflaha", "قَدْ أَفْلَح"));  //18
        arrayList.add(new QuranChapterandVerses( "19", 5796, 6484,"Wa Qalallazina", "وَقَالَ الَّذِينَ"));  //19
        arrayList.add(new QuranChapterandVerses( "20", 6486, 6820,"A'man Khalaq", "أَمَّنْ خَلَق"));  //20


        arrayList.add(new QuranChapterandVerses( "21", 6822, 7188,"Utlu Ma Oohi", "اتْلُ مَا أُوحِيَ"));  //21
        arrayList.add(new QuranChapterandVerses( "22", 7190, 7518,"Wa Manyaqnut", "وَمَنْ يَقْنُت"));  //22
        arrayList.add(new QuranChapterandVerses( "23", 7520, 8250,"Wa Mali", "وَمَا لِيَ"));  //23
        arrayList.add(new QuranChapterandVerses( "24", 8252, 8604,"Faman Azlam", "فَمَنْ أَظْلَم"));  //24
        arrayList.add(new QuranChapterandVerses( "25", 8606, 9106,"Elahe Yuruddo", "إِلَيْهِ يُرَدُّ"));  //25


        arrayList.add(new QuranChapterandVerses( "26", 9108, 9506,"Ha'a Meem", "حم"));  //26
        arrayList.add(new QuranChapterandVerses( "27", 9508, 10318,"Qala Fama Khatbukum", "قَالَ فَمَا خَطْبُكُم"));  //27
        arrayList.add(new QuranChapterandVerses( "28", 10320, 10610,"Qadd Sami Allah", "قَدْ سَمِعَ اللَّه"));  //28
        arrayList.add(new QuranChapterandVerses( "29", 10612, 11492,"Tabarakallazi", "تَبَارَكَ الَّذِي"));  //29
        arrayList.add(new QuranChapterandVerses( "30", 11496, 12695,"Amma Yatasa'aloon", "عَمَّ يَتَسَاءَلُون")); //30
        return arrayList;
    }


    public static ArrayList<QuranChapterandVerses> arrChapterSurat(Context context) {
        ArrayList<QuranChapterandVerses> arrayList = new ArrayList<>();

        /* 1*/
        arrayList.add(new QuranChapterandVerses("001", 0, 13, "Surah Al-Fatihah", "7 verses", "Meccan", "(the Opening)"));
        /* 2*/
        arrayList.add(new QuranChapterandVerses("002", 14, 586, "Surah Al-Baqarah", "286 verses", "Medinan", "(the Cow)"));
        /* 3*/
        arrayList.add(new QuranChapterandVerses("003", 588, 988, "Surah Aali Imran", "200 verses", "Medinan", "(the Family of Imran)"));
        /* 4*/
        arrayList.add(new QuranChapterandVerses("004", 990, 1342, "Surah An-Nisa’", "176 verses", "Medinan", "(the Women)"));
        /* 5*/
        arrayList.add(new QuranChapterandVerses("005", 1344, 1584, "Surah Al-Ma’idah", "120 verses", "Medinan", "(the Table)"));
        /* 6*/
        arrayList.add(new QuranChapterandVerses("006", 1586, 1916, "Surah Al-An’am", "165 verses", "Meccan", "(the Cattle)"));
        /* 7*/
        arrayList.add(new QuranChapterandVerses("007", 1918, 2330, "Surah Al-A’raf", "206 verses", "Meccan", "(the Heights)"));
        /* 8*/
        arrayList.add(new QuranChapterandVerses("008", 2332, 2482, "Surah Al-Anfal", "75 verses", "Medinan", "(the Spoils of War)"));
        /* 9*/
        arrayList.add(new QuranChapterandVerses("009", 2484, 2740, "Surah At-Taubah", "129 verses", "Medinan", "(the Repentance)"));

        /* 10*/
        arrayList.add(new QuranChapterandVerses("010", 2742, 2960, "Surah Yunus", "109 verses", "Meccan", "(Yunus)"));
        /* 11*/
        arrayList.add(new QuranChapterandVerses("011", 2962,3208 , "Surah Hud", "123 verses", "Meccan", "(Hud)"));
        /* 12*/
        arrayList.add(new QuranChapterandVerses("012", 3210, 3432, "Surah Yusuf", "111 verses", "Meccan", "(Yusuf)"));
        /* 13*/
        arrayList.add(new QuranChapterandVerses("013", 3434, 3520, "Surah Ar-Ra’d", "43 verses", "Medinan", "(the Thunder)"));
        /* 14*/
        arrayList.add(new QuranChapterandVerses("014", 3522, 3626, "Surah Ibrahim", "52 verses", "Meccan", "(Ibrahim)"));
        /* 15*/
        arrayList.add(new QuranChapterandVerses("015", 3628, 3826, "Surah Al-Hijr", "99 verses", "Meccan", "(the Rocky Tract)"));
        /* 16*/
        arrayList.add(new QuranChapterandVerses("016", 3828, 4084, "Surah An-Nahl", "128 verses", "Meccan", "(the Bees)"));
        /* 17*/
        arrayList.add(new QuranChapterandVerses("017", 4086, 4308, "Surah Al-Isra’", "111 verses", "Meccan", "(the Night Journey)"));
        /* 18*/
        arrayList.add(new QuranChapterandVerses("018", 4310, 4530, "Surah Al-Kahf", "110 verses", "Meccan", "(the Cave)"));
        /* 19*/
        arrayList.add(new QuranChapterandVerses("019", 4532, 4728, "Surah Maryam", "98 verses", "Meccan", "(Maryam)"));

        /* 20*/
        arrayList.add(new QuranChapterandVerses("020", 4730, 5000, "Surah Ta-Ha", "135 verses", "Meccan", "(Ta-Ha)"));
        /* 21*/
        arrayList.add(new QuranChapterandVerses("021", 5002, 5226, "Surah Al-Anbiya’", "112 verses", "Meccan", "(the Prophets)"));
        /* 22*/
        arrayList.add(new QuranChapterandVerses("022", 5228, 5384, "Surah Al-Haj", "78 verses", "Medinan", "(the Pilgrimage)"));
        /* 23*/
        arrayList.add(new QuranChapterandVerses("023", 5386, 5622, "Surah Al-Mu’minun", "118 verses", "Meccan", "(the Believers)"));
        /* 24*/
        arrayList.add(new QuranChapterandVerses("024", 5624, 5754, "Surah An-Nur", "64 verses", "Medinan", "(the Light)"));
        /* 25*/
        arrayList.add(new QuranChapterandVerses("025", 5754, 5910, "Surah Al-Furqan", "77 verses", "Meccan", "(the Criterion)"));
        /* 26*/
        arrayList.add(new QuranChapterandVerses("026", 5910, 6366, "Surah Ash-Shu’ara’", "227 verses", "Meccan", "(the Poets)"));
        /* 27*/
        arrayList.add(new QuranChapterandVerses("027", 6366, 6552, "Surah An-Naml", "93 verses", "Meccan", "(the Ants)"));
        /* 28*/
        arrayList.add(new QuranChapterandVerses("028", 6554, 6730, "Surah Al-Qasas", "88 verses", "Meccan", "(the Stories)"));
        /* 29*/
        arrayList.add(new QuranChapterandVerses("029", 6732, 6870, "Surah Al-Ankabut", "69 verses", "Meccan", "(the Spider)"));

        /* 30*/
        arrayList.add(new QuranChapterandVerses("030", 6872, 6992, "Surah Ar-Rum", "60 verses", "Meccan", "(the Romans)"));
        /* 31*/
        arrayList.add(new QuranChapterandVerses("031", 6994, 7062, "Surah Luqman", "34 verses", "Meccan", "(Luqman)"));
        /* 32*/
        arrayList.add(new QuranChapterandVerses("032", 7064, 7124, "Surah As-Sajdah", "30 verses", "Meccan", "(the Prostration)"));
        /* 33*/
        arrayList.add(new QuranChapterandVerses("033", 7126, 7272, "Surah Al-Ahzab", "73 verses", "Medinan", "(the Combined Forces)"));
        /* 34*/
        arrayList.add(new QuranChapterandVerses("034", 7274, 7382, "Surah Saba’", "54 verses", "Meccan", "(the Sabeans)"));
        /* 35*/
        arrayList.add(new QuranChapterandVerses("035", 7384, 7474, "Surah Al-Fatir", "45 verses", "Meccan", "(the Originator)"));
        /* 36*/
        arrayList.add(new QuranChapterandVerses("036", 7476, 7642, "Surah Ya-Sin", "83 verses", "Meccan", "(Ya-Sin)"));
        /* 37*/
        arrayList.add(new QuranChapterandVerses("037", 7644, 8008, "Surah As-Saffah", "182 verses", "Meccan", "(Those Ranges in Ranks)"));
        /* 38*/
        arrayList.add(new QuranChapterandVerses("038", 8010, 8186, "Surah Sad", "88 verses", "Meccan ", "(Sad)"));
        /* 39*/
        arrayList.add(new QuranChapterandVerses("039", 8188, 8338, "Surah Az-Zumar", "75 verses", "Meccan", "(the Groups)"));

        /* 40*/
        arrayList.add(new QuranChapterandVerses("040", 8340, 8510, "Surah Ghafar", " 85 verses", "Meccan", "(the Forgiver)"));
        /* 41*/
        arrayList.add(new QuranChapterandVerses("041", 8512, 8620, "Surah Fusilat", " 54 verses", "Meccan", "(Distinguished)"));
        /* 42*/
        arrayList.add(new QuranChapterandVerses("042", 8622, 8728, "Surah Ash-Shura", " 53 verses", "Meccan", "(the Consultation)"));
        /* 43*/
        arrayList.add(new QuranChapterandVerses("043", 8730, 8908, "Surah Az-Zukhruf", " 89 verses", "Meccan", "(the Gold)"));
        /* 44*/
        arrayList.add(new QuranChapterandVerses("044", 8910, 9028, "Surah Ad-Dukhan", " 59 verses", "Meccan", "(the Smoke)"));
        /* 45*/
        arrayList.add(new QuranChapterandVerses("045", 9030, 9158, "Surah Al-Jathiyah", " 37 verses", "Meccan", "(the Kneeling)"));
        /* 46*/
        arrayList.add(new QuranChapterandVerses("046", 9160, 9176, "Surah Al-Ahqaf", " 35 verses", "Meccan", "(the Valley)"));
        /* 47*/
        arrayList.add(new QuranChapterandVerses("047", 9178, 9254, "Surah Muhammad", " 38 verses", "Medinan", "(Muhammad)"));
        /* 48*/
        arrayList.add(new QuranChapterandVerses("048", 9256, 9314, "Surah Al-Fat’h", " 29 verses", "Medinan", "(the Victory)"));
        /* 49*/
        arrayList.add(new QuranChapterandVerses("049", 9316, 9352, "Surah Al-Hujurat", "18  verses", "Medinan", "(the Dwellings)"));

        /* 50*/
        arrayList.add(new QuranChapterandVerses("050", 9354, 9444, "Surah Qaf", "45 verses", "Meccan", "(Qaf)"));
        /* 51*/
        arrayList.add(new QuranChapterandVerses("051", 9446, 9566, "Surah Adz-Dzariyah", "60 verses", "Meccan", "(the Scatterers)"));
        /* 52*/
        arrayList.add(new QuranChapterandVerses("052", 9568, 9666, "Surah At-Tur", "49  verses", "Meccan", "(the Mount)"));
        /* 53*/
        arrayList.add(new QuranChapterandVerses("053", 9668, 9792, "Surah An-Najm", "62 verses", "Meccan", "(the Star)"));
        /* 54*/
        arrayList.add(new QuranChapterandVerses("054", 9794, 9904, "Surah  Al-Qamar", "55  verses", "Meccan", "(the Moon)"));
        /* 55*/
        arrayList.add(new QuranChapterandVerses("055", 9906, 10062, "Surah Ar-Rahman", "78 verses", "Medinan", "(the Most Gracious)"));
        /* 56*/
        arrayList.add(new QuranChapterandVerses("056", 10064, 10256, "Surah Al-Waqi’ah", "96 verses", "Meccan", "(the Event)"));
        /* 57*/
        arrayList.add(new QuranChapterandVerses("057", 10258, 10316, "Surah Al-Hadid", "29 verses", "Medinan", "(the Iron)"));
        /* 58*/
        arrayList.add(new QuranChapterandVerses("058", 10318, 10362, "Surah Al-Mujadilah", "22 verses", "Medinan", "(the Reasoning)"));
        /* 59*/
        arrayList.add(new QuranChapterandVerses("059", 10364, 10412, "Surah   Al-Hashr", "24 verses", "Medinan", "(the Gathering)"));


        /* 60*/
        arrayList.add(new QuranChapterandVerses("060", 10414, 10440, "Surah Al-Mumtahanah", "13 verses", "Medinan", "(the Tested)"));
        /* 61*/
        arrayList.add(new QuranChapterandVerses("061", 10442, 10470, "Surah As-Saf", "14 verses", "Medinan", "(the Row)"));
        /* 62*/
        arrayList.add(new QuranChapterandVerses("062", 10472, 10494, "Surah Al-Jum’ah", "11 verses", "Medinan", "(Friday)"));
        /* 63*/
        arrayList.add(new QuranChapterandVerses("063", 10496, 10518, "Surah Al-Munafiqun", "11 verses", "Medinan", "(the Hypocrites)"));
        /* 64*/
        arrayList.add(new QuranChapterandVerses("064", 10520, 10556, "Surah At-Taghabun", "18 verses", "Medinan", "(the Loss & Gain)"));
        /* 65*/
        arrayList.add(new QuranChapterandVerses("065", 10558, 10582, "Surah At-Talaq", "12 verses", "Medinan", "(the Divorce)"));
        /* 66*/
        arrayList.add(new QuranChapterandVerses("066", 10584, 10608, "Surah At-Tahrim", "12 verses", "Medinan", "(the Prohibition)"));
        /* 67*/
        arrayList.add(new QuranChapterandVerses("067", 10610, 10670, "Surah Al-Mulk", "30 verses", "Meccan", "(the Kingdom)"));
        /* 68*/
        arrayList.add(new QuranChapterandVerses("068", 10672, 10776, "Surah Al-Qalam", "52 verses", "Meccan", "(the Pen)"));
        /* 69*/
        arrayList.add(new QuranChapterandVerses("069", 10778, 10882, "Surah Al-Haqqah", "52 verses", "Meccan", "(the Inevitable)"));


        /* 70*/
        arrayList.add(new QuranChapterandVerses("070", 10884, 10972, "Surah Al-Ma’arij", "44 verses", "Meccan", "(the Elevated Passages)"));
        /* 71*/
        arrayList.add(new QuranChapterandVerses("071", 10974, 11030, "Surah Nuh", "28 verses", "Meccan", "(Nuh)"));
        /* 72*/
        arrayList.add(new QuranChapterandVerses("072", 11032, 11092, "Surah Al-Jinn", "28 verses", "Meccan", "(the Jinn)"));
        /* 73*/
        arrayList.add(new QuranChapterandVerses("073", 11094, 11130, "Surah Al-Muzammil", "20 verses", "Meccan", "(the Wrapped)"));
        /* 74*/
        arrayList.add(new QuranChapterandVerses("074", 11132, 11244, "Surah Al-Mudaththir", "56 verses", "Meccan", "(the Cloaked)"));
        /* 75*/
        arrayList.add(new QuranChapterandVerses("075", 11246, 11326, "Surah Al-Qiyamah", "40 verses", "Meccan", "(the Resurrection)"));
        /* 76*/
        arrayList.add(new QuranChapterandVerses("076", 11328, 11390, "Surah Al-Insan", "31 verses", "Medinan", "(the Human)"));
        /* 77*/
        arrayList.add(new QuranChapterandVerses("077", 11392, 11492, "Surah Al-Mursalat", "50 verses", "Meccan", "(Those Sent Forth)"));
        /* 78*/
        arrayList.add(new QuranChapterandVerses("078", 11494, 11574, "Surah An-Naba’", "40 verses", "Meccan", "(the Great News)"));
        /* 79*/
        arrayList.add(new QuranChapterandVerses("079", 11576, 11668, "Surah An-Nazi’at", "46 verses", "Meccan", "(Those Who Pull Out)"));

        /* 80*/
        arrayList.add(new QuranChapterandVerses("080", 11670, 11754, "Surah ‘Abasa", "42 verses", "Meccan", "(He Frowned)"));
        /* 81*/
        arrayList.add(new QuranChapterandVerses("081", 11756, 11814, "Surah At-Takwir", "29 verses", "Meccan", "(the Overthrowing)"));
        /* 82*/
        arrayList.add(new QuranChapterandVerses("082", 11816, 11854, "Surah Al-Infitar", "19 verses", "Meccan", "(the Cleaving)"));
        /* 83*/
        arrayList.add(new QuranChapterandVerses("083", 11856, 11928, "Surah Al-Mutaffifin", "36 verses", "Meccan", "(Those Who Deal in Fraud)"));
        /* 84*/
        arrayList.add(new QuranChapterandVerses("084", 11930, 11980, "Surah Al-Inshiqaq", "25 verses", "Meccan", "(the Splitting Asunder)"));
        /* 85*/
        arrayList.add(new QuranChapterandVerses("085", 11982, 12026, "Surah Al-Buruj", "22 verses", "Meccan", "(the Stars)"));
        /* 86*/
        arrayList.add(new QuranChapterandVerses("086", 12028, 12062, "Surah At-Tariq", "17 verses", "Meccan", "(the Nightcomer)"));
        /* 87*/
        arrayList.add(new QuranChapterandVerses("087", 12064, 12102, "Surah Al-A’la", "19 verses", "Meccan", "(the Most High)"));
        /* 88*/
        arrayList.add(new QuranChapterandVerses("088", 12104, 12156, "Surah Al-Ghashiyah", "26 verses", "Meccan", "(the Overwhelming)"));
        /* 89*/
        arrayList.add(new QuranChapterandVerses("089", 12158, 12218, "Surah Al-Fajr", "30 verses", "Meccan", "(the Dawn)"));


        /* 90*/
        arrayList.add(new QuranChapterandVerses("090", 12220, 12260, "Surah Al-Balad", "20 verses", "Meccan", "(the City)"));
        /* 91*/
        arrayList.add(new QuranChapterandVerses("091", 12262, 12292, "Surah Ash-Shams", "15 verses", "Meccan", "(the Sun)"));
        /* 92*/
        arrayList.add(new QuranChapterandVerses("092", 12294, 12336, "Surah Al-Layl", "21 verses", "Meccan", "(the Night)"));
        /* 93*/
        arrayList.add(new QuranChapterandVerses("093", 12338, 12260, "Surah Adh-Dhuha", "11 verses", "Meccan", "(the Forenoon)"));
        /* 94*/
        arrayList.add(new QuranChapterandVerses("094", 12262, 12378, "Surah Al-Inshirah", "8 verses", "Meccan", "(the Opening Forth)"));
        /* 95*/
        arrayList.add(new QuranChapterandVerses("095", 12380, 12396, "Surah At-Tin", "8 verses", "Meccan", "(the Fig)"));
        /* 96*/
        arrayList.add(new QuranChapterandVerses("096", 12398, 12436, "Surah Al-‘Alaq", "19  verses", "Meccan", "(the Clot)"));
        /* 97*/
        arrayList.add(new QuranChapterandVerses("097", 12438, 12448, "Surah Al-Qadar", "5 verses", "Meccan", "(the Night of Decree)"));
        /* 98*/
        arrayList.add(new QuranChapterandVerses("098", 12450, 12466, "Surah Al-Bayinah", "8 verses", "Medinan", "(the Proof)"));
        /* 99*/
        arrayList.add(new QuranChapterandVerses("099", 12468, 12484, "Surah Az-Zalzalah", "8 verses", "Medinan", "(the Earthquake)"));

        /* 100*/
        arrayList.add(new QuranChapterandVerses("100", 12486, 12508, "Surah Al-‘Adiyah", "11 verses", "Meccan", "(the Runners)"));

        /* 101*/
        arrayList.add(new QuranChapterandVerses("101", 12510, 12532, "Surah Al-Qari’ah", "11 verses", "Meccan", "(the Striking Hour)"));
        /* 102*/
        arrayList.add(new QuranChapterandVerses("102", 12534, 12550, "Surah At-Takathur", "8 verses", "Meccan", "(the Piling Up)"));
        /* 103*/
        arrayList.add(new QuranChapterandVerses("103", 12552, 12558, "Surah Al-‘Asr", "3 verses", "Meccan", "(the Time)"));
        /* 104*/
        arrayList.add(new QuranChapterandVerses("104", 12560, 12578, "Surah Al-Humazah", "9 verses", "Meccan", "(the Slanderer)"));
        /* 105*/
        arrayList.add(new QuranChapterandVerses("105", 12580, 12590, "Surah Al-Fil", "5 verses", "Meccan", "(the Elephant)"));
        /* 106*/
        arrayList.add(new QuranChapterandVerses("106", 12592, 12600, "Surah Quraish", "4 verses", "Meccan", "(Quraish)"));
        /* 107*/
        arrayList.add(new QuranChapterandVerses("007", 12602, 12616, "Surah Al-Ma’un", "7 verses", "Meccan", "(the Assistance)"));
        /* 108*/
        arrayList.add(new QuranChapterandVerses("108", 12618, 12624, "Surah Al-Kauthar", "3 verses", "Meccan", "(the River of Abundance)"));
        /* 109*/
        arrayList.add(new QuranChapterandVerses("109", 12626, 12638, "Surah Al-Kafirun", "6 verses", "Meccan", "(the Disbelievers)"));

        /* 110*/
        arrayList.add(new QuranChapterandVerses("110", 12640, 12646, "Surah An-Nasr", "3 verses", "Medinan", "(the Help)"));
        /* 111*/
        arrayList.add(new QuranChapterandVerses("111", 12648, 12658, "Surah Al-Masad", "5 verses", "Medinan", "(the Palm Fiber)"));
        /* 112*/
        arrayList.add(new QuranChapterandVerses("112", 12660, 12668, "Surah Al-Ikhlas", "4 verses", "Meccan", "(the Sincerity)"));
        /* 113*/
        arrayList.add(new QuranChapterandVerses("113", 12670, 12680, "Surah Al-Falaq", "5 verses", "Meccan", "(the Daybreak)"));
        /* 114*/
        arrayList.add(new QuranChapterandVerses("114", 12682, 12695, "Surah An-Nas", "6 verses", "Meccan", "(Mankind)"));

        return arrayList;
    }


}
