package wamztech.quranenglish.constatnts;

import android.os.Environment;

import wamztech.quranenglish.BaseApplication;


/**
 * Created by khanhamza on 4/20/2017.
 */

public class AppConstants {

    public static final String INPUT_DATE_FORMAT = "yyyy-dd-MM hh:mm:ss";
    public static final String INPUT_DATE_FORMAT_AM_PM = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_DATE_FORMAT = "EEEE dd,yyyy";
    public static final String INPUT_TIME_FORMAT = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_TIME_FORMAT = "hh:mm a";
    public static final String OUTPUT_DATE_TIME_FORMAT = "EEEE dd,yyyy hh:mm a";
    public static String DEVICE_OS_ANDROID = "ANDROID";
    public static final String GENERAL_DATE_FORMAT = "dd-MM-yy";
    public static final String ROOT_MEDIA_PATH = Environment.getExternalStorageDirectory().getPath()
            + "/" + BaseApplication.getApplicationName() + "/Media";
    public static final String USER_PROFILE_PICTURE = ROOT_MEDIA_PATH + "/" + BaseApplication.getApplicationName() + " Profile";

    /*******************Preferences KEYS******************/
    public static final String USER_DATA = "userData";
    public static final String USER_NOTIFICATION_DATA = "USER_NOTIFICATION_DATA";

    public static String PROFILE_REGISTRATION = "profile_registration";
    public static String FORCED_RESTART = "forced_restart";
    public static final String KEY_QURAN_CHAPTERS = "KEY_QURAN_CHAPTERS";
//    public static final String KEY_SURAT = "KEY_SURAT";


    /*******************Preferences KEYS******************/
}
