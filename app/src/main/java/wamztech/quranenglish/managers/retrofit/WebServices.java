package wamztech.quranenglish.managers.retrofit;

import android.app.Activity;
import android.content.Context;

import com.kaopiz.kprogresshud.KProgressHUD;
import wamztech.quranenglish.helperclasses.Helper;
import wamztech.quranenglish.helperclasses.ui.helper.UIHelper;
import wamztech.quranenglish.models.wrappers.WebResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by hamzakhan on 6/30/2017.
 */

public class WebServices {
    private WebServiceProxy apiService;
    private KProgressHUD mDialog;
    private Context mContext;


    public WebServices(Activity activity, String token, boolean isShowDialog) {
        apiService = WebServiceFactory.getInstance(token);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }



    private void dismissDialog() {
        mDialog.dismiss();
    }


    public void webServiceLang(String lang, IRequestWebResponseJustObjectCallBack iRequestWebResponseJustObjectCallBack) {
        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                WebServiceFactory.getInstance("").
                        authenticateUser("http://anaxdesigns.website/quran/?get=languages")
                        .enqueue(new Callback<WebResponse<Object>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                                dismissDialog();

                                if (response.body() == null) {
                                    iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
//                                    iRequestWebResponseJustObjectCallBack.onError();
                                } else {
                                    iRequestWebResponseJustObjectCallBack.requestDataResponse(response.body());
//                                    iRequestWebResponseJustObjectCallBack.requestDataResponse(response.raw().request().url());
                                }
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
//                                iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
                                UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                                dismissDialog();

                            }
                        });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

    }

    public void webServiceGeneric(String lang, String reader, String chapter, String verse, IRequestWebResponseJustObjectCallBack iRequestWebResponseJustObjectCallBack) {
        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                WebServiceFactory.getInstance("").
                        authenticateUser("http://anaxdesigns.website/quran/?get=verse&lang="+lang+"&reader="+reader+"&chapter="+chapter+"&verse="+verse)
                        .enqueue(new Callback<WebResponse<Object>>() {
                            @Override
                            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                                dismissDialog();

                                if (response.body() == null) {
                                    iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
//                                    iRequestWebResponseJustObjectCallBack.onError();
                                } else {
                                    iRequestWebResponseJustObjectCallBack.requestDataResponse(response.body());
//                                    iRequestWebResponseJustObjectCallBack.requestDataResponse(response.raw().request().url());
                                }
                            }

                            @Override
                            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                                iRequestWebResponseJustObjectCallBack.onError((WebResponse<Object>) call);
                                UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                                dismissDialog();

                            }
                        });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

    }
    public interface IRequestWebResponseJustObjectCallBack {
        void requestDataResponse(WebResponse<Object> webResponse);
        void onError(WebResponse<Object> webResponse);
//        void onError();
    }


}
