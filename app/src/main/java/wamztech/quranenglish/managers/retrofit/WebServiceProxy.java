package wamztech.quranenglish.managers.retrofit;


import wamztech.quranenglish.models.wrappers.WebResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by khanhamza on 09-Mar-17.
 */

public interface WebServiceProxy {



    @GET
    Call<WebResponse<Object>>  authenticateUser(@Url String url);
}

