package wamztech.quranenglish.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;


import wamztech.quranenglish.R;
import wamztech.quranenglish.adapters.recyleradapters.BookMarkAdapter;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;
import wamztech.quranenglish.database.DBHelper;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.models.ContentModel;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


import static android.view.View.VISIBLE;

public class BookmarkedFragment extends BaseFragment implements OnItemAdapterClickListener {

    Unbinder unbinder;
    BookMarkAdapter adapBookMarked;
    ArrayList<ContentModel> arrBookedMark;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.contPlayer)
    LinearLayout contPlayer;
    private ContentModel contentModel;

    DBHelper mydb;


    public static BookmarkedFragment newInstance(ContentModel contentModel) {

        Bundle args = new Bundle();

        BookmarkedFragment fragment = new BookmarkedFragment();
        fragment.contentModel = contentModel;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrBookedMark = new ArrayList<>();
        mydb = new DBHelper(getBaseActivity());
        adapBookMarked = new BookMarkAdapter(getBaseActivity(), arrBookedMark, this);


    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        arrBookedMark.clear();
        arrBookedMark.addAll(mydb.getAllData());
        bindView();
        contPlayer.setVisibility(View.GONE);
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle("Bookmarked");
        titleBar.showBackButton(getBaseActivity());
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rv.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rv.getItemAnimator()).setSupportsChangeAnimations(false);
        rv.setAdapter(adapBookMarked);
        adapBookMarked.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(String parent, View view, int position, Object object) {
        if (object instanceof ContentModel) {
            ContentModel contentModel = (ContentModel) object;


            switch (view.getId()) {
                case R.id.btnCross: {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity());
                    builder.setMessage(R.string.deleteContact)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mydb.deleteContact(contentModel.getChapterNumber(), contentModel.getSuratNo(),
                                            contentModel.getAyatNo(), contentModel.getArabic(), contentModel.getEng());
                                    Toast.makeText(getContext(), "Deleted Successfully",
                                            Toast.LENGTH_SHORT).show();
                                    getBaseActivity().refreshFragment(BookmarkedFragment.newInstance(contentModel));
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });

                    AlertDialog d = builder.create();
                    d.setTitle("Are you sure");
                    d.show();

                }
                break;
                case R.id.contContent:
                        getBaseActivity().addDockableFragment(DetailFragment.newInstance(null, contentModel), false);
                    break;
            }


        }
    }
}
