package wamztech.quranenglish.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class DashboardFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    @BindView(R.id.btnQuran)
    AnyTextView btnQuran;
    @BindView(R.id.btnBookMarked)
    AnyTextView btnBookMarked;
    public static DashboardFragment newInstance() {
        Bundle args = new Bundle();
        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
        titleBar.setTitle("Dashboard");
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnQuran, R.id.btnBookMarked})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnQuran:
                getBaseActivity().addDockableFragment(HomeTabLayout.newInstance(), false);

                break;
            case R.id.btnBookMarked:
                getBaseActivity().addDockableFragment(BookmarkedFragment.newInstance(null),false);
                break;
        }
    }
}