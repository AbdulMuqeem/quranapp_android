package wamztech.quranenglish.fragments.dialogs;


import android.os.Build;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class TranslationDialogFragment extends DialogFragment {


    Unbinder unbinder;
    @BindView(R.id.contArabic)
    LinearLayout contArabic;
    @BindView(R.id.contBoth)
    LinearLayout contBoth;
    @BindView(R.id.contTranslation)
    LinearLayout contTranslation;
    private OnItemAdapterClickListener onItemAdapterClickListener;


    public TranslationDialogFragment() {
    }

    public static TranslationDialogFragment newInstance(OnItemAdapterClickListener onItemAdapterClickListener) {
        TranslationDialogFragment frag = new TranslationDialogFragment();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.onItemAdapterClickListener = onItemAdapterClickListener;

        return frag;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.popup_translation, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindData();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void bindData() {
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.contArabic, R.id.contBoth, R.id.contTranslation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contArabic:
                onItemAdapterClickListener.onItemClick(null, view, 0, "Arabic");
                dismiss();
                break;
            case R.id.contBoth:
                onItemAdapterClickListener.onItemClick(null, view, 0, "both");
                dismiss();
                break;
            case R.id.contTranslation:
                onItemAdapterClickListener.onItemClick(null, view, 0, "English");
                dismiss();
                break;
        }
    }
}

