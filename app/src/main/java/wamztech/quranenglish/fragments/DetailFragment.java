package wamztech.quranenglish.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;


import wamztech.quranenglish.R;
import wamztech.quranenglish.adapters.recyleradapters.ContentAdapter;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;
import wamztech.quranenglish.database.DBHelper;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.fragments.dialogs.BookMarkedDialogFragment;
import wamztech.quranenglish.fragments.dialogs.TranslationDialogFragment;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.helperclasses.ui.helper.UIHelper;
import wamztech.quranenglish.managers.retrofit.GsonFactory;
import wamztech.quranenglish.managers.retrofit.WebServices;
import wamztech.quranenglish.models.AudioModel;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.models.QuranChapterandVerses;
import wamztech.quranenglish.models.SpinnerDetail;
import wamztech.quranenglish.models.SpinnerList;
import wamztech.quranenglish.models.wrappers.WebResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


import static android.view.View.VISIBLE;

public class DetailFragment extends BaseFragment implements OnItemAdapterClickListener, AdapterView.OnItemSelectedListener {

    Unbinder unbinder;
    @BindView(R.id.rv)
    RecyclerView rv;
    ArrayList<AudioModel> arrAudio, arrEngArUrl;
    ArrayList<String> arrCompleteUrls;
    ArrayList<String> arrText, arrTexten, arrTextar;
    ArrayList<ContentModel> arrCompleteQuran;
    ContentAdapter adapContent;
    @BindView(R.id.btnPlayPrev)
    ImageView btnPlayPrev;
    @BindView(R.id.btnPlayCurr)
    ToggleButton btnPlay;
    @BindView(R.id.btnPlayNext)
    ImageView btnPlayNext;
    @BindView(R.id.spQari)
    Spinner spQari;
    @BindView(R.id.contPlayMedia)
    LinearLayout contPlayMedia;

    private ContentModel contentModel;
    private MediaPlayer player;
    private QuranChapterandVerses chapterandVerses;
    private boolean isSuccess = true;
    private DBHelper mydb;
    private int position = 0;
    ArrayList<SpinnerDetail> arrSpinner;
    private String QariName;
    private String lang = "arabic";
    ArrayList<String> arrArab;
    ArrayList<String> arrEng;
    private int globalPos;

    public static DetailFragment newInstance(QuranChapterandVerses chapterandVerses, ContentModel contentModel) {

        Bundle args = new Bundle();

        DetailFragment fragment = new DetailFragment();
        fragment.contentModel = contentModel;
        fragment.chapterandVerses = chapterandVerses;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrText = new ArrayList<>();
        arrTexten = new ArrayList<>();
        arrTextar = new ArrayList<>();
        arrAudio = new ArrayList<>();
        arrEngArUrl = new ArrayList<>();
        arrCompleteUrls = new ArrayList<>();
        arrCompleteQuran = new ArrayList<>();
        arrArab = new ArrayList<>();
        arrEng = new ArrayList<>();
        arrSpinner = new ArrayList<>();
        adapContent = new ContentAdapter(getBaseActivity(), arrCompleteQuran, this);
        mydb = new DBHelper(getBaseActivity());

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_detail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        readFile();
        bindView();
        spinner();
        QariName = "mishary-rashid-alafasy";
        spQari.setOnItemSelectedListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        killMediaPlayer();
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.showBackButton(getBaseActivity());
        if (contentModel != null)
            titleBar.setTitle(contentModel.getName());
        else
            titleBar.setTitle(chapterandVerses.getChapterNameEng());

    }

    @Override
    public void setListeners() {
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (!btnPlay.isChecked()) {
                    killMediaPlayer();
                } else {
                    itemClick(position, true);


                }
            }
        });


    }


    private void readFile() {
        int permissionCheck = ContextCompat.checkSelfPermission(getBaseActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getBaseActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    5);
        }


        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(getBaseActivity().getAssets().open("arQuranv1.txt")));
//            BufferedReader br_en = new BufferedReader(new InputStreamReader(getBaseActivity().getAssets().open("enQuranv1.txt")));
            BufferedReader br_en = new BufferedReader(new InputStreamReader(getBaseActivity().getAssets().open("new_trans.txt")));
            String line;
            String line_en;

            while (((line = br.readLine()) != null) && (line_en = br_en.readLine()) != null) {
                arrText.add(line);
                arrText.add(line_en);
            }


            br.close();
            br_en.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        if (contentModel != null) {
            setData();
        } else {
            String eng, arab;
            for (int i = chapterandVerses.getVersesStartFrom(); i < chapterandVerses.getVersesEndsTo() + 1; i += 2) {
                String[] content = arrText.get(i).split(":");
                String[] content_eng = arrText.get(i + 1).split(":");

                String[] content1 = content[0].split(",");
                int ayat = Integer.parseInt(content1[1]);
                int surat1 = Integer.parseInt(content1[0]);
                arab = (content[1]);
                eng = (content_eng[1]);

                arrCompleteQuran.add(new ContentModel("", chapterandVerses.getChapterNumber(), surat1, ayat, arab, eng,
                        chapterandVerses.getChapterNameEng(), chapterandVerses.getVersesStartFrom(), chapterandVerses.getVersesEndsTo()));
            }
        }
    }

    private void setData() {
        String eng, arab;

        for (int i = contentModel.getVersesStartFrom(); i < contentModel.getVersesEndsTo() + 1; i += 2) {

            String[] content = arrText.get(i).split(":");
            String[] content_eng = arrText.get(i + 1).split(":");

            String[] content1 = content[0].split(",");
            int ayat = Integer.parseInt(content1[1]);
            int surat1 = Integer.parseInt(content1[0]);
            arab = (content[1]);
            eng = (content_eng[1]);

            if (contentModel != null) {
                arrCompleteQuran.add(new ContentModel(contentModel.getTextValue(), contentModel.getChapterNumber(), surat1, ayat, arab, eng,
                        contentModel.getName(), contentModel.getVersesStartFrom(), contentModel.getVersesEndsTo()));
                //                rv.scrollToPosition(contentModel.getAyatNo());
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        killMediaPlayer();
    }


    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rv.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rv.getItemAnimator()).setSupportsChangeAnimations(false);
        rv.setAdapter(adapContent);
        adapContent.notifyDataSetChanged();

        if (contentModel != null)
            scrollToPosition(contentModel.getAyatNo());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onItemClick(String txtValue, View view, int position, Object object) {
        if (object instanceof ContentModel) {
            ContentModel contentModel = (ContentModel) object;
//            adapContent.isClickable = false;
            switch (view.getId()) {
                case R.id.imgBookMark:

                    BookMarkedDialogFragment bookMarkedDialogFragment = BookMarkedDialogFragment.newInstance(new OnItemAdapterClickListener() {
                        @Override
                        public void onItemClick(String txtValue, View view, int position, Object object) {
                            if (object.equals("Ok")) {
                                contentModel.setTextValue(txtValue);
                                db(contentModel);
                            }

                        }
                    });
                    bookMarkedDialogFragment.setCancelable(true);
                    bookMarkedDialogFragment.show(getBaseActivity().getSupportFragmentManager(), null);

                    break;
                case R.id.imgShare:

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "Surah " + chapterandVerses.getChapterNameEng() + " verse " + contentModel.getAyatNo() + "\n" + contentModel.getArabic() + "\n" + contentModel.getEng();
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Verse");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    break;

                case R.id.contContent:

                    adapContent.isClickable = false;
                    globali = position;
                    globalPos = position;
                    itemClick(position, false);


                    break;


            }
        } else {
            switch (view.getId()) {

                case R.id.contArabic:
                    UIHelper.showToast(getBaseActivity(), "Arabic");
                    lang = "arabic";
                    break;
                case R.id.contBoth:
                    UIHelper.showToast(getBaseActivity(), "both");
                    lang = "both";
                    break;
                case R.id.contTranslation:
                    UIHelper.showToast(getBaseActivity(), "English");
                    lang = "english";
                    QariName = "default";
                    break;
            }
        }
    }


    public void scrollToPosition(int scrollToPosition) {
        if (scrollToPosition > -1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    rv.smoothScrollToPosition(scrollToPosition);
                }
            }, 500);
        } else {
            rv.scrollToPosition(0);
        }

    }


    private void killMediaPlayer() {
        if (player != null) {
            try {
                if (player != null) player.release();

//                btnPlay.setChecked(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    int globali = 0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick({R.id.btnPlayPrev, R.id.btnPlayNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnPlayPrev:
                if (globali > 0) {
                    globali--;
                    initializeMediaPlayerAra(/*arrCompleteUrls.get(globali),*/ arrCompleteUrls);
                    btnPlay.setChecked(true);
                }

                break;
            case R.id.btnPlayNext:
                if (globali < arrCompleteUrls.size()) {
                    globali++;
                    initializeMediaPlayerAra(/*arrCompleteUrls.get(globali), */arrCompleteUrls);
                    btnPlay.setChecked(true);
                }
                break;
        }
    }


    private void servicCallAll(int chapterNumber, int verse, boolean allVerses, String qariName, String language) {
        String verses = String.format("%03d", verse);
        String chap = String.format("%03d", chapterNumber);
        new WebServices(getBaseActivity(),
                "",
                true)
                .webServiceGeneric(language, qariName, chap, verses,
                        new WebServices.IRequestWebResponseJustObjectCallBack() {
                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                if (webResponse.responseCode == 200) {

                                    AudioModel audio = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), AudioModel.class);
                                    arrCompleteUrls.clear();

                                    if (lang.equalsIgnoreCase("both")) {

                                        if (language.equalsIgnoreCase("Arabic")) {
                                            for (int pos = 0; pos < arrCompleteQuran.size(); pos++) {
                                                String reg = (String) audio.getAudio().subSequence(audio.getAudio().length() - 15, audio.getAudio().length());
                                                arrArab.add(audio.getAudio().split(reg)[0] +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "/" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "-" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getAyatNo()) + ".mp3");
//                                                }
                                            }
                                        } else {
                                            for (int pos = 0; pos < arrCompleteQuran.size(); pos++) {
                                                String reg = (String) audio.getAudio().subSequence(audio.getAudio().length() - 15, audio.getAudio().length());
                                                arrEng.add(audio.getAudio().split(reg)[0] +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "/" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "-" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getAyatNo()) + ".mp3");


                                            }

                                        }
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                sortArray(arrArab, arrEng);

                                            }
                                        }, 5000);

                                    } else {
                                        for (int pos = 0; pos < arrCompleteQuran.size(); pos++) {
                                            String reg = (String) audio.getAudio().subSequence(audio.getAudio().length() - 15, audio.getAudio().length());
                                            arrCompleteUrls.add(audio.getAudio().split(reg)[0] +
                                                    String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "/" +
                                                    String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "-" +
                                                    String.format("%03d", arrCompleteQuran.get(pos).getAyatNo()) + ".mp3");
                                        }

                                        initializeMediaPlayerAra(/*arrCompleteUrls.get(0), */arrCompleteUrls);
                                    }

                                    btnPlay.setChecked(true);


                                } else
                                    UIHelper.showShortToastInCenter(getContext(), "Something went wrong");
                                isSuccess = false;
                            }

                            @Override
                            public void onError(WebResponse<Object> webResponse) {
                                UIHelper.showShortToastInCenter(getContext(), "Something went wrong");
                                isSuccess = true;
                            }

                        });


    }

    private void servicCalItem(int chapterNumber, int verse, boolean allVerses, String qariName, String language) {
        killMediaPlayer();
        arrCompleteUrls.clear();
        arrArab.clear();
        arrEng.clear();
        String verses = String.format("%03d", verse);
        String chap = String.format("%03d", chapterNumber);
        new WebServices(getBaseActivity(),
                "",
                true)
                .webServiceGeneric(language, qariName, chap, verses,
                        new WebServices.IRequestWebResponseJustObjectCallBack() {
                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                if (webResponse.responseCode == 200) {

                                    AudioModel audio = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), AudioModel.class);
                                    arrCompleteUrls.clear();

                                    if (lang.equalsIgnoreCase("both")) {

                                        if (language.equalsIgnoreCase("Arabic")) {
                                            for (int pos = 0; pos < arrCompleteQuran.size(); pos++) {
                                                String reg = (String) audio.getAudio().subSequence(audio.getAudio().length() - 15, audio.getAudio().length());
                                                arrArab.add(audio.getAudio().split(reg)[0] +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "/" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "-" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getAyatNo()) + ".mp3");
//                                                }
                                            }
                                        } else {
                                            for (int pos = 0; pos < arrCompleteQuran.size(); pos++) {
                                                String reg = (String) audio.getAudio().subSequence(audio.getAudio().length() - 15, audio.getAudio().length());
                                                arrEng.add(audio.getAudio().split(reg)[0] +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "/" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "-" +
                                                        String.format("%03d", arrCompleteQuran.get(pos).getAyatNo()) + ".mp3");


                                            }

                                        }
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                sortArray1(arrArab, arrEng, verse);

                                            }
                                        }, 5000);

                                    } else {
                                        for (int pos = 0; pos < arrCompleteQuran.size(); pos++) {
                                            String reg = (String) audio.getAudio().subSequence(audio.getAudio().length() - 15, audio.getAudio().length());
                                            arrCompleteUrls.add(audio.getAudio().split(reg)[0] +
                                                    String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "/" +
                                                    String.format("%03d", arrCompleteQuran.get(pos).getSuratNo()) + "-" +
                                                    String.format("%03d", arrCompleteQuran.get(pos).getAyatNo()) + ".mp3");
                                        }

                                        initializeMediaPlayerAra1(/*arrCompleteUrls.get(0), */arrCompleteUrls);
                                    }

                                    btnPlay.setChecked(true);


                                } else
                                    UIHelper.showShortToastInCenter(getContext(), "Something went wrong");
                                isSuccess = false;
                            }

                            @Override
                            public void onError(WebResponse<Object> webResponse) {
                                UIHelper.showShortToastInCenter(getContext(), "Something went wrong");
                                isSuccess = true;
                            }

                        });


    }


    /*
     * DataBase
     * */
    private void db(ContentModel contentModel) {


        if (mydb.insertContact(contentModel.getTextValue(), contentModel.getChapterNumber(), contentModel.getSuratNo(),
                contentModel.getAyatNo(), contentModel.getArabic(), contentModel.getEng(),
                contentModel.getName(), contentModel.getVersesEndsTo(), contentModel.getVersesStartFrom())) {
            Toast.makeText(getBaseActivity(), "BookedMarked",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getBaseActivity(), "Please try again",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void spinner() {


        new WebServices(getBaseActivity(),
                "",
                true)
                .webServiceLang("languages",
                        new WebServices.IRequestWebResponseJustObjectCallBack() {
                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                if (webResponse.responseCode == 200) {
                                    SpinnerList spinnerList = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), SpinnerList.class);

                                    arrSpinner.addAll(spinnerList.getArabic());
                                    ArrayAdapter<SpinnerDetail> adapter = new ArrayAdapter<>(getBaseActivity(),
                                            R.layout.item_spinner, arrSpinner);

                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spQari.setAdapter(adapter);

                                }
                            }

                            @Override
                            public void onError(WebResponse<Object> webResponse) {
//                                UIHelper.showShortToastInCenter(getContext(), "Something went wrong, API error");
                            }

                        });


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        SpinnerDetail spinnerDetail = (SpinnerDetail) parent.getSelectedItem();
        QariName = spinnerDetail.getReader();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void itemClick(int itemPosition, boolean allVerses) {
        TranslationDialogFragment translationDialogFragment = TranslationDialogFragment.newInstance(new OnItemAdapterClickListener() {
            @Override
            public void onItemClick(String parent, View view, int position, Object object) {
                if (object.equals("English")) {
                    lang = "english";
                    if (allVerses)
                        servicCallAll(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), false, "default", "english");
                    else
                        servicCalItem(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), false, "default", "english");

                } else if (object.equals("Arabic")) {
                    lang = "arabic";
                    if (allVerses)
                        servicCallAll(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), false, QariName, "arabic");
                    else
                        servicCalItem(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), false, QariName, "arabic");

                } else {

                    lang = "both";
                    if (allVerses) {
                        servicCallAll(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), true, QariName, "arabic");
                        servicCallAll(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), true, "default", "english");
                    } else {
                        servicCalItem(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), true, QariName, "arabic");
                        servicCalItem(arrCompleteQuran.get(itemPosition).getSuratNo(), arrCompleteQuran.get(itemPosition).getAyatNo(), true, "default", "english");
                    }

                }

            }

        });
        translationDialogFragment.setCancelable(true);
        adapContent.notifyDataSetChanged();
        translationDialogFragment.show(getBaseActivity().getSupportFragmentManager(), null);

    }


    @Override
    public void onClick(View view) {

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void sortArray(ArrayList<String> arrArab, ArrayList<String> arrEng) {
        arrAudio.clear();

        if (arrArab.size() > 0) {
            // ## Ascending order
            // To compare string values
            Collections.sort(arrArab, String::compareToIgnoreCase);
        }
        if (arrEng.size() > 0) {
            // ## Ascending order
            // To compare string values
            Collections.sort(arrEng, String::compareToIgnoreCase);
        }


        for (int i = 0; i < arrArab.size(); i++) {
            arrCompleteUrls.add(arrArab.get(i));
            arrCompleteUrls.add(arrEng.get(i));
        }
        globali = 0;

        initializeMediaPlayerAra(/*arrCompleteUrls.get(0),*/ arrCompleteUrls);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void sortArray1(ArrayList<String> arrArab, ArrayList<String> arrEng, int verse) {
        arrAudio.clear();

        if (arrArab.size() > 0) {
            // ## Ascending order
            // To compare string values
            Collections.sort(arrArab, String::compareToIgnoreCase);
        }
        if (arrEng.size() > 0) {
            // ## Ascending order
            // To compare string values
            Collections.sort(arrEng, String::compareToIgnoreCase);
        }

        arrCompleteUrls.clear();
        for (int i = 0; i < arrArab.size(); i++) {
            arrCompleteUrls.add(arrArab.get(i));
            arrCompleteUrls.add(arrEng.get(i));
        }
        globali = globalPos * 2;

        initializeMediaPlayerAra1(/*arrCompleteUrls.get(0),*/ arrCompleteUrls);
    }


    private void initializeMediaPlayerAra(ArrayList urlS) {
        if (!lang.equalsIgnoreCase("both")) {
            for (ContentModel contentModel : arrCompleteQuran) {
                contentModel.setSelected(false);
            }
            arrCompleteQuran.get(globali).setSelected(true);
            adapContent.notifyDataSetChanged();
        }
        else {
            for (ContentModel contentModel : arrCompleteQuran) {
                contentModel.setSelected(false);
            }
            arrCompleteQuran.get(globali/2).setSelected(true);
            adapContent.notifyDataSetChanged();
        }
        if (globali < urlS.size()) {
            killMediaPlayer();
            player = new MediaPlayer();
            player.setAudioAttributes(new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build());

            try {
                player.setDataSource(getBaseActivity(), Uri.parse(urlS.get(globali).toString()));
                player.prepare();
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        player.stop();
                        globali++;

                        if (globali < urlS.size()) {
                            initializeMediaPlayerAra(/*urlS.get(globali).toString(),*/ urlS);

                            if (!lang.equalsIgnoreCase("both")) {
                                for (ContentModel contentModel : arrCompleteQuran) {
                                    contentModel.setSelected(false);
                                }
                                arrCompleteQuran.get(globali).setSelected(true);
                                adapContent.notifyDataSetChanged();
                                scrollToPosition(globali);
                            }
                            else {
                                for (ContentModel contentModel : arrCompleteQuran) {
                                    contentModel.setSelected(false);
                                }
                                arrCompleteQuran.get(globali/2).setSelected(true);
                                adapContent.notifyDataSetChanged();
                                scrollToPosition(globali/2);
                            }
                        } else {
                            btnPlay.setChecked(false);
//                        globali = 0;
                            killMediaPlayer();
                        }
                    }
                });
                player.start();


            } catch (IllegalArgumentException | IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void initializeMediaPlayerAra1(ArrayList urlS) {
        if (!lang.equalsIgnoreCase("both")) {
            for (ContentModel contentModel : arrCompleteQuran) {
                contentModel.setSelected(false);
            }
            arrCompleteQuran.get(globali).setSelected(true);
            adapContent.notifyDataSetChanged();
        } else {
            for (ContentModel contentModel : arrCompleteQuran) {
                contentModel.setSelected(false);
            }
            arrCompleteQuran.get(globali/2).setSelected(true);
            adapContent.notifyDataSetChanged();
        }

        if (globali < urlS.size()) {
            killMediaPlayer();
            player = new MediaPlayer();
            player.setAudioAttributes(new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build());

            try {
                player.setDataSource(getBaseActivity(), Uri.parse(urlS.get(globali).toString()));
                player.prepare();
                player.setOnCompletionListener(mp -> {
                    player.stop();
                    globali++;
//                    globalPos++;

                    if (globali < urlS.size()) {
                        initializeMediaPlayerAra1(/*urlS.get(globali).toString(),*/ urlS);
                        if (!lang.equalsIgnoreCase("both")) {
                            for (ContentModel contentModel : arrCompleteQuran) {
                                contentModel.setSelected(false);
                            }
                            arrCompleteQuran.get(globali).setSelected(true);
                            adapContent.notifyDataSetChanged();
                            scrollToPosition(globali);
                        }
                        else {
                            for (ContentModel contentModel : arrCompleteQuran) {
                                contentModel.setSelected(false);
                            }
                            arrCompleteQuran.get(globali/2).setSelected(true);
                            adapContent.notifyDataSetChanged();
                            scrollToPosition(globali/2);
                        }
                    } else {
                        btnPlay.setChecked(false);
                        killMediaPlayer();
                    }
                });
                player.start();


            } catch (IllegalArgumentException | IOException e) {
                e.printStackTrace();
            }
        } else {
            killMediaPlayer();
        }
    }


}


