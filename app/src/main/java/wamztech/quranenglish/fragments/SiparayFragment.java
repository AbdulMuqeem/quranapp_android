package wamztech.quranenglish.fragments;

import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;


import wamztech.quranenglish.R;
import wamztech.quranenglish.adapters.recyleradapters.SiparayNameAdapter;
import wamztech.quranenglish.callbacks.OnItemClickListener;
import wamztech.quranenglish.constatnts.Constants;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.models.QuranChapterandVerses;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


import static android.view.View.VISIBLE;


public class SiparayFragment extends BaseFragment implements View.OnClickListener, OnItemClickListener {
    @BindView(R.id.rv_generic)
    RecyclerView rvGeneric;
    Unbinder unbinder;
    SiparayNameAdapter siparayNameAdapter;

    ArrayList<String> arrText;
    ArrayList<ContentModel> arrContent;

    public static SiparayFragment newInstance() {

        Bundle args = new Bundle();

        SiparayFragment fragment = new SiparayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrText = new ArrayList<>();
        arrContent = new ArrayList<>();
        siparayNameAdapter = new SiparayNameAdapter(getBaseActivity(), Constants.arrSiparay(getBaseActivity()), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView();
    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_generic_recylcer_view;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.showBackButton(getBaseActivity());
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle("Quran");
        titleBar.showHome(getBaseActivity());
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvGeneric.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rvGeneric.getItemAnimator()).setSupportsChangeAnimations(false);
        rvGeneric.setAdapter(siparayNameAdapter);
        siparayNameAdapter.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(int position, Object object) {
        if (object instanceof QuranChapterandVerses) {
            QuranChapterandVerses siparay = (QuranChapterandVerses) object;
//            sharedPreferenceManager.putObject(AppConstants.KEY_QURAN_CHAPTERS, siparay);

            getBaseActivity().addDockableFragment(DetailFragment.newInstance(siparay, null), false);

        }
    }


}
