package wamztech.quranenglish.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;



import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.adapters.recyleradapters.SearchAdapter;
import wamztech.quranenglish.callbacks.OnItemAdapterClickListener;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.helperclasses.ui.helper.KeyboardHelper;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.models.Siparay;
import wamztech.quranenglish.models.Surat;
import wamztech.quranenglish.widget.AnyEditTextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


import static android.view.View.VISIBLE;

public class SearchFragment extends BaseFragment implements OnItemAdapterClickListener {

    Unbinder unbinder;
    @BindView(R.id.rv)
    RecyclerView rv;

    ArrayList<String> arrText;
    ArrayList<ContentModel> arrContent;
    ArrayList<ContentModel> arrSearchList;
    SearchAdapter adapContent;
    @BindView(R.id.edtSearchBar)
    AnyEditTextView edtSearchBar;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.imgCancel)
    ImageView imgCancel; @BindView(R.id.contList)
    LinearLayout contList;
    private boolean isSearchBarEmpty = true;

    private Siparay siparay;
    private boolean isFromSiparay;
    private Surat surat;

    public static SearchFragment newInstance() {

        Bundle args = new Bundle();

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrContent = new ArrayList<>();
        arrSearchList = new ArrayList<>();
        arrText = new ArrayList<>();
        adapContent = new SearchAdapter(getBaseActivity(), arrSearchList, this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contList.setVisibility(View.GONE);
        readFile();
        bindView();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_search;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle("Quran");
        titleBar.showHome(getBaseActivity());
    }

    @Override
    public void setListeners() {
        edtSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapContent.getFilter().filter(charSequence);
//                if (edtSearchBar.getStringTrimmed().equalsIgnoreCase("")) {
//                    isSearchBarEmpty = true;
//                    imgSearch.setImageResource(R.drawable.icon_search_updated);
//
//                } else {
//                    isSearchBarEmpty = false;
//                    imgSearch.setImageResource(R.drawable.icon_cross);
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        edtSearchBar.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN)
            {
                switch (keyCode)
                {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        contList.setVisibility(VISIBLE);
                        return true;
                    default:
                        break;
                }
            }
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (isSearchBarEmpty) {
//            imgSearch.setImageResource(R.drawable.icon_search_updated);
//
//        } else {
//            imgSearch.setImageResource(R.drawable.icon_cross);
//        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    private void readFile() {
        int permissionCheck = ContextCompat.checkSelfPermission(getBaseActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getBaseActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    5);
        }


        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(getBaseActivity().getAssets().open("arQuranv1.txt")));
            BufferedReader br_en = new BufferedReader(new InputStreamReader(getBaseActivity().getAssets().open("enQuranv1.txt")));
            String line;
            String line_en;

            while (((line = br.readLine()) != null) && (line_en = br_en.readLine()) != null) {
                arrText.add(line);
                arrText.add(line_en);
//                        arrText.add("");
            }
            br.close();
            br_en.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        String eng, arab;
        for (int i = 0; i < arrText.size(); i += 2) {

            String[] content = arrText.get(i).split(":");
            String[] content_eng = arrText.get(i + 1).split(":");

            String[] content1 = content[0].split(",");
            int ayat = Integer.parseInt(content1[1]);
            int surat = Integer.parseInt(content1[0]);
            arab = (content[1]);
            eng = (content_eng[1]);
            arrContent.add(new ContentModel("001", surat, ayat, arab, eng));

        }
        arrSearchList.clear();
        arrSearchList.addAll(arrContent);

    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rv.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rv.getItemAnimator()).setSupportsChangeAnimations(false);
        rv.setAdapter(adapContent);
        adapContent.notifyDataSetChanged();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.imgSearch, R.id.imgCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
                contList.setVisibility(VISIBLE);
                break;
            case R.id.imgCancel:
                contList.setVisibility(View.GONE);
                edtSearchBar.setText("");
                KeyboardHelper.hideSoftKeyboard(getContext(), edtSearchBar);
                break;
        }
    }

    @Override
    public void onItemClick(String txtValue, View view, int position, Object object) {

    }
}
