package wamztech.quranenglish.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;


import wamztech.quranenglish.R;
import wamztech.quranenglish.adapters.recyleradapters.SuratNameAdapter;
import wamztech.quranenglish.callbacks.OnItemClickListener;
import wamztech.quranenglish.constatnts.Constants;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.models.ContentModel;
import wamztech.quranenglish.models.QuranChapterandVerses;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


import static android.view.View.VISIBLE;


public class SuratFragment extends BaseFragment implements View.OnClickListener, OnItemClickListener {
    @BindView(R.id.rv_generic)
    RecyclerView rvGeneric;
    Unbinder unbinder;
    SuratNameAdapter adapSurat;
    ArrayList<String> arrText;
    ArrayList<ContentModel> arrContent;

    public static SuratFragment newInstance() {

        Bundle args = new Bundle();

        SuratFragment fragment = new SuratFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrText = new ArrayList<>();
        arrContent = new ArrayList<>();
//        adapSurat = new SuratNameAdapter(getBaseActivity(), Constants.arrSurat(getBaseActivity()), this);
        adapSurat = new SuratNameAdapter(getBaseActivity(), Constants.arrChapterSurat(getBaseActivity()), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_generic_recylcer_view;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle("Quran");
        titleBar.showHome(getBaseActivity());
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvGeneric.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rvGeneric.getItemAnimator()).setSupportsChangeAnimations(false);
        rvGeneric.setAdapter(adapSurat);
        adapSurat.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(int position, Object object) {
        if (object instanceof QuranChapterandVerses) {
            QuranChapterandVerses quranChapterandVerses = (QuranChapterandVerses) object;
//            sharedPreferenceManager.putObject(AppConstants.KEY_QURAN_CHAPTERS, quranChapterandVerses);
            getBaseActivity().addDockableFragment(DetailFragment.newInstance(quranChapterandVerses, null), false);

        }
    }


}
