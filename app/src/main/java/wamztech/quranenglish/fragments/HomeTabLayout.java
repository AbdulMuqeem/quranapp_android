package wamztech.quranenglish.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.android.material.tabs.TabLayout;

import wamztech.quranenglish.R;
import wamztech.quranenglish.adapters.HomeTabPagerAdapter;
import wamztech.quranenglish.fragments.abstracts.BaseFragment;
import wamztech.quranenglish.helperclasses.ui.helper.TitleBar;
import wamztech.quranenglish.widget.CustomViewPager;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


import static android.view.View.VISIBLE;

public class HomeTabLayout extends BaseFragment {

    Unbinder unbinder;
    HomeTabPagerAdapter tabPagerAdapter;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    CustomViewPager viewpager;


    public static HomeTabLayout newInstance(/*boolean isFromTimeline, int patientVisitAdmissionID*/) {

        Bundle args = new Bundle();

        HomeTabLayout fragment = new HomeTabLayout();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_tablayout_home;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle("Quran");
        titleBar.showHome(getBaseActivity());
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setViewPagerAdapter();
    }

    private void setViewPagerAdapter() {
        viewpager.setPagingEnabled(true);
        viewpager.setOffscreenPageLimit(2);
        tabPagerAdapter = new HomeTabPagerAdapter(getChildFragmentManager());
        viewpager.setAdapter(tabPagerAdapter);

        tabs.setupWithViewPager(viewpager);
        tabs.setTabMode(TabLayout.MODE_FIXED);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
