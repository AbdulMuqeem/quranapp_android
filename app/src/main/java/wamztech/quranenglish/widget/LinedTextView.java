package wamztech.quranenglish.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import android.util.AttributeSet;

import com.ctrlplusz.anytextview.Util;

import wamztech.quranenglish.R;
import wamztech.quranenglish.utility.Utils;

import androidx.appcompat.widget.AppCompatTextView;


public class LinedTextView extends AppCompatTextView {


    private Rect mRect;
    private Paint mPaint;

    public LinedTextView(Context context) {
        super(context);
        drawLine();
    }

    public LinedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Util.setTypeface(attrs, this);
        drawLine();
    }

    public LinedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Utils.setTypeface(attrs, this);
        drawLine();
    }

    private void drawLine() {
        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(getResources().getColor(R.color.star_grey));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int count = getLineCount();
        Rect r = mRect;
        Paint paint = mPaint;

        for (int i = 0; i < count; i++) {
            int baseline = getLineBounds(i, r);

            canvas.drawLine(r.left, baseline + 6, r.right, baseline + 6, paint);
        }

        super.onDraw(canvas);
    }

    public String getStringTrimmed() {
        return getText().toString().trim();
    }


}
