package wamztech.quranenglish.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import wamztech.quranenglish.models.ContentModel;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String QURAN_TABLE_NAME = "quran";
    public static final String QURAN_COLUMN_TEXTVALUE = "textValue";
    public static final String QURAN_COLUMN_SURAT_NO = "surat";
    public static final String QURAN_COLUMN_SIPARA_NO = "sipara";
    public static final String QURAN_COLUMN_AYAT_NO = "ayat";
    private static final String QURAN_COLUMN_ENGLISH = "english";
    public static final String QURAN_COLUMN_ARABIC = "arabic";
    private static final String QURAN_COLUMN_NAME = "Name";
    public static final String QURAN_COLUMN_START_FROM = "startfrom";
    public static final String QURAN_COLUMN_ENDSON = "endson";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL("create table quran " + "(id integer primary key,textValue text, surat integer,sipara integer,ayat integer, english text,arabic text,Name text, startfrom integer,endson integer)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS quran");
        onCreate(db);
    }

    public boolean insertContact(String textValue, String siparaNo, int suratNo, int ayatNo, String arabic, String eng, String name, int endson, int startfrom) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("textValue", textValue);
        contentValues.put("sipara", siparaNo);
        contentValues.put("surat", suratNo);
        contentValues.put("ayat", ayatNo);
        contentValues.put("arabic", arabic);
        contentValues.put("english", eng);
        contentValues.put("Name", name);
        contentValues.put("startfrom", startfrom);
        contentValues.put("endson", endson);
        db.insert("quran", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res = db.rawQuery("select * from quran where id=" + id + "", null);
        Cursor res = db.rawQuery("select * from quran where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, QURAN_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact(Integer id, String name, String phone, String email, String street, String place) {
        SQLiteDatabase db = this.getWritableDatabase();

//        ContentValues contentValues = new ContentValues();
//        contentValues.put("name", name);
//        contentValues.put("phone", phone);
//        contentValues.put("email", email);
//        contentValues.put("street", street);
//        contentValues.put("place", place);
//
//        db.update("quran", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Integer deleteContact(String siparaNo, int suratNo, int ayatNo, String arabic, String eng) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(QURAN_TABLE_NAME, QURAN_COLUMN_ENGLISH + "=?", new String[]{eng});

    }


    public ArrayList<ContentModel> getAllData() {
        ArrayList<ContentModel> array_list = new ArrayList<ContentModel>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("select * from quran", null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(new ContentModel(
                    res.getString(res.getColumnIndex(QURAN_COLUMN_TEXTVALUE)),
                    res.getString(res.getColumnIndex(QURAN_COLUMN_SIPARA_NO)),
                    res.getInt(res.getColumnIndex(QURAN_COLUMN_SURAT_NO)),
                    res.getInt(res.getColumnIndex(QURAN_COLUMN_AYAT_NO)),
                    res.getString(res.getColumnIndex(QURAN_COLUMN_ARABIC)),
                    res.getString(res.getColumnIndex(QURAN_COLUMN_ENGLISH)),
                    res.getString(res.getColumnIndex(QURAN_COLUMN_NAME)),
                    res.getInt(res.getColumnIndex(QURAN_COLUMN_START_FROM)),
                    res.getInt(res.getColumnIndex(QURAN_COLUMN_ENDSON))));

            res.moveToNext();
        }

        return array_list;

    }

}