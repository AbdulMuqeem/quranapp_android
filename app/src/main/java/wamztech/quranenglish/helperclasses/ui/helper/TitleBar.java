package wamztech.quranenglish.helperclasses.ui.helper;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import wamztech.quranenglish.R;
import wamztech.quranenglish.activities.BaseActivity;
import wamztech.quranenglish.activities.HomeActivity;
import wamztech.quranenglish.widget.AnyTextView;



/**
 * Created by khanhamza on 02-Mar-17.
 */

public class TitleBar extends RelativeLayout {


    final Handler handler = new Handler();
    private LinearLayout containerTitlebar1;
    private AnyTextView btnLeft1;
    private AnyTextView txtTitle;
    private AnyTextView btnRight2;


    public TitleBar(Context context) {
        super(context);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }


    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.titlebar_main, this);
        bindViews();
    }

    private void bindViews() {
        txtTitle = findViewById(R.id.txtTitle);
        btnLeft1 = findViewById(R.id.btnLeft1);
        btnRight2 = findViewById(R.id.btnRight2);

        containerTitlebar1 = findViewById(R.id.containerTitlebar1);

    }

    public void resetViews() {
        containerTitlebar1.setVisibility(VISIBLE);
        btnLeft1.setVisibility(INVISIBLE);
        btnRight2.setVisibility(INVISIBLE);

    }


    public void setTitle(String title) {

        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(title);
    }

    public void showBackButton(final Activity mActivity) {
        this.btnLeft1.setVisibility(VISIBLE);
        this.btnLeft1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_back, 0, 0, 0);
        this.btnLeft1.setText(null);
        btnLeft1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity != null) {
//                    mActivity.getSupportFragmentManager().popBackStack();
                    mActivity.onBackPressed();
                }

            }
        });
    }


    public void showHome(final BaseActivity activity) {
// FIXME: 5/25/2018 txtView--- ImageButton
        this.btnLeft1.setVisibility(VISIBLE);
        this.btnRight2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity instanceof HomeActivity) {
//                    activity.reload();
                    activity.popStackTill(1);
//                    activity.notifyToAll(ON_HOME_PRESSED, TitleBar.this);

                } else {
                    activity.clearAllActivitiesExceptThis(HomeActivity.class);
                }
            }
        });
    }



}
